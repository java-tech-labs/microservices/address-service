package com.microservices.address.service.impl;

import com.microservices.address.entity.Address;
import com.microservices.address.exception.AddressNotFoundException;
import com.microservices.address.mapper.AddressMapper;
import com.microservices.address.payload.AddressDTO;
import com.microservices.address.repository.AddressRepository;
import com.microservices.address.service.AddressService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressServiceImpl(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    @Override
    public List<AddressDTO> getAllAddress() {
        List<Address> addresses = addressRepository.findAll();
        return addressMapper.mapToDTOList(addresses);
    }

    @Override
    public AddressDTO getAddressByEmployeeId(Integer employeeId) {
        Address address = addressRepository.findAddressByEmployeeId(employeeId);

        if (address == null)
            throw new AddressNotFoundException("Address with employee ID " + employeeId + " not found");

        return addressMapper.mapToDTO(address);
    }
}
