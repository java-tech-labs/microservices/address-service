package com.microservices.address.service;

import com.microservices.address.payload.AddressDTO;

import java.util.List;

public interface AddressService {

    AddressDTO getAddressByEmployeeId(Integer employeeId);

    List<AddressDTO> getAllAddress();
}
