package com.microservices.address.repository;

import com.microservices.address.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

    @Query(nativeQuery = true, value = "select a.id, a.line_1, a.line_2, a.state, a.zip " +
            "from addresses a join employees e " +
            "on a.employee_id  = e.id where a.employee_id =:employeeId")
    Address findAddressByEmployeeId(@Param("employeeId") Integer employeeId);
}
