package com.microservices.address.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDTO {

    private Integer id;
    private String line1;
    private String line2;
    private Long zip;
    private String state;
}
